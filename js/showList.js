"use strict";

const firstTestArr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const secondTestArr = ["1", "2", "3", "sea", "user", 23];
const thirdTestArr = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
const fourthTestArr = ["Kharkiv", "Kiev", ["Borispol", "Irpin", ["Odessa", "Lviv"]], "Dnieper"];

function changeArrStrToArrHtml (input) {
    return input.map(elem => {
        if(Array.isArray(elem)) return `<ul>${changeArrStrToArrHtml(elem)}</ul>`;
        return `<li>${elem}</li>`;
    }).join('');
}

function showListOnPage (input, parent = document.body) {
    parent.insertAdjacentHTML('beforeend', `<ul>${changeArrStrToArrHtml(input)}</ul>`);
}

// alternative solution
// function showListOnPage(input, parent = document.body) {
//     if (Array.isArray(input)) {
//         const newList = document.createElement("ul");
//
//         input.map(item => {
//             showListOnPage(item, newList);
//         })
//         parent.append(newList);
//         return newList;
//     }
//
//         const newLi = document.createElement(`li`);
//         newLi.textContent = input;
//         parent.append(newLi);
// }

function clearPageTimer(time) {
    const timerBlock = document.createElement('p');
    timerBlock.style.backgroundColor = "red";
    timerBlock.style.fontSize = "50px";
    timerBlock.style.textAlign = "center";

    let timerInterval = setInterval(() => {
        if (time > 0) {
            timerBlock.innerText = 'Cleaning after: ' + time-- + 's';
            document.body.append(timerBlock);
        } else {
            document.body.innerHTML = '';
            clearInterval(timerInterval);
        }

    }, 1000)
}

showListOnPage(firstTestArr);
showListOnPage(secondTestArr);
showListOnPage(thirdTestArr);
showListOnPage(fourthTestArr);
clearPageTimer(5);






